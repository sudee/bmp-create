#ifndef CONFIG_H
#define CONFIG_H

typedef struct {
    int width, height;
    int seed;
    char* output;
} Config;

Config cfg_create(int argc, char** argv);
void print_info(Config* cfg);

#endif /* CONFIG_H */
