#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "bitmap.h"

void rorschach(Bitmap* bmp);
void mandelbrot(Bitmap* bmp);
void dragon(Bitmap* bmp);
void binary(Bitmap* bmp);
void experiment(Bitmap* bmp);

#endif /* ALGORITHM_H */
