#ifndef COLOR_H
#define COLOR_H

typedef unsigned char uint8_t;

typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} Color;

Color color_create(uint8_t r, uint8_t g, uint8_t b);
void color_set(Color* c, uint8_t r, uint8_t g, uint8_t b);
void color_print(Color* c);
void color_invert(Color* c);
int color_to_int(Color* c);

#endif /* COLOR_H */
