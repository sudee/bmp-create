#include <stdio.h>
#include <stdlib.h>
#include "bitmap.h"
#include "algorithm.h"
#include "config.h"


int main(int argc, char** argv) {
    Config cfg = cfg_create(argc, argv);
    print_info(&cfg);
    srand(cfg.seed);

    Bitmap bmp;
    bmp_create(&bmp, cfg.width, cfg.height);

    // rorschach(&bmp);
    dragon(&bmp);

    FILE* f = fopen(cfg.output, "wb");
    bmp_write(&bmp, f);
    bmp_destroy(&bmp);
    fclose(f);

    return 0;
}
