#ifndef BITMAP_H
#define BITMAP_H

#include <stdio.h>
#include "color.h"

typedef struct {
    int height;
    int width;
    int filesize;
    Color* pixels;
} Bitmap;

void bmp_create(Bitmap* bmp, int width, int height);
void bmp_set_pixel(Bitmap* bmp, int x, int y, Color c);
Color bmp_get_pixel(Bitmap* bmp, int x, int y);
void bmp_read(Bitmap* bmp, FILE* f);
void bmp_write(Bitmap* bmp, FILE* f);
void bmp_destroy(Bitmap* bmp);

#endif /* BITMAP_H */
