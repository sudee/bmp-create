CC=gcc
CFLAGS=-Wall -pedantic -std=c99 -O1
OBJECTS=main.o color.o bitmap.o config.o algorithm.o

bmpcreate.exe: $(OBJECTS)
	$(CC) $(OBJECTS) -o $@

.PHONY : clean

clean:
	del *.exe *.o
