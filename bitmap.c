#include <stdlib.h>
#include "bitmap.h"

#define OLDRBYTES(X) ((X) & 0x000000FF), ((X) & 0x0000FF00) >> 8, ((X) & 0x00FF0000) >> 16, ((X) & 0xFF000000) >> 24
#define RBYTES(X) (X) & 255, (X) >> 8 & 255, (X) >> 16 & 255, (X) >> 24 & 255

#define FILE_HEADER_LEN  14
#define INFO_HEADER_LEN  40
#define BYTE_PADDING    (bmp->width % 4)
#define PIXEL_ARRAY_LEN ((bmp->width * 3 + BYTE_PADDING) * bmp->height)
#define PIXEL_START     (FILE_HEADER_LEN + INFO_HEADER_LEN)


void bmp_create(Bitmap* bmp, int width, int height) {
    bmp->pixels = (Color*) malloc(sizeof(Color) * width * height);
    bmp->width = width;
    bmp->height = height;
}

void bmp_set_pixel(Bitmap* bmp, int x, int y, Color c) {
    if (y >= bmp->height || y < 0 || x >= bmp->width || x < 0) return;
    int i = bmp->width * y + x;
    bmp->pixels[i] = c;
}

Color bmp_get_pixel(Bitmap* bmp, int x, int y) {
    int i = bmp->width * y + x;
    return bmp->pixels[i];
}

void bmp_read(Bitmap* bmp, FILE* f) {
    uint8_t file_header[FILE_HEADER_LEN];
    uint8_t info_header[INFO_HEADER_LEN];
    uint8_t* pixel_array = (uint8_t*) malloc(sizeof(uint8_t) * PIXEL_ARRAY_LEN);
    
}

void bmp_write(Bitmap* bmp, FILE* f) {
    int byte_padding = bmp->width % 4;
    int pixel_array_len = (bmp->width * 3 + byte_padding) * bmp->height;
    int pixel_start = FILE_HEADER_LEN + INFO_HEADER_LEN;
    int filesize = pixel_start + pixel_array_len;

    uint8_t file_header[FILE_HEADER_LEN] = {
        0x42, 0x4D,
        RBYTES(filesize),
        0x00, 0x00, 0x00, 0x00,
        RBYTES(pixel_start)
    };
    uint8_t info_header[INFO_HEADER_LEN] = {
        0x28, 0x00, 0x00, 0x00,
        RBYTES(bmp->width),
        RBYTES(bmp->height),
        0x01, 0x00, 0x18, 0x00,
        0x00, 0x00, 0x00, 0x00,
        RBYTES(pixel_array_len),
        0x13, 0x0B, 0x00, 0x00,
        0x13, 0x0B, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00
    };

    uint8_t* pixel_array = (uint8_t*) malloc(sizeof(uint8_t) * pixel_array_len);
    uint8_t* p = pixel_array;
    for (int y = bmp->height - 1; y >= 0; y--) {
        for (int x = 0; x < bmp->width; x++) {
            Color c = bmp_get_pixel(bmp, x, y);
            *(p++) = c.blue;
            *(p++) = c.green;
            *(p++) = c.red;
        }
        for (int i = 0; i < byte_padding; i++) *(p++) = 0;
    }

    fwrite(file_header, sizeof(uint8_t), FILE_HEADER_LEN, f);
    fwrite(info_header, sizeof(uint8_t), INFO_HEADER_LEN, f);
    fwrite(pixel_array, sizeof(uint8_t), pixel_array_len, f);

    free(pixel_array);
}

void bmp_destroy(Bitmap* bmp) {
    free(bmp->pixels);
}
