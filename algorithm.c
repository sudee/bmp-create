#include <stdlib.h>
#include <complex.h>
#include "algorithm.h"
#define IN_RANGE(X, A, B) ((X) >= (A) && (X) < (B))
#define RAND(A, B) (((double) rand() / (RAND_MAX + 1)) * ((B) - (A) + 1) + (A))

void rorschach(Bitmap* bmp) {
    Color bg, dot, glow;
    color_set(&dot, 0, 0, 255);
    color_set(&glow, 0, 140, 210);
    color_set(&bg, 30, 30, 50);

    int w = bmp->width, h = bmp->height;

    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            bmp_set_pixel(bmp, x, y, bg);
        }
    }

    int x = w / 2;
    int y = h / 2;
    for (int i = 0; i < 9000; i++) {
        bmp_set_pixel(bmp, x, y, dot);
        bmp_set_pixel(bmp, x-1, y, glow);
        bmp_set_pixel(bmp, x+1, y, glow);
        bmp_set_pixel(bmp, x, y-1, glow);
        bmp_set_pixel(bmp, x, y+1, glow);

        bmp_set_pixel(bmp, w-x, y, dot);
        bmp_set_pixel(bmp, w-x-1, y, glow);
        bmp_set_pixel(bmp, w-x+1, y, glow);
        bmp_set_pixel(bmp, w-x, y-1, glow);
        bmp_set_pixel(bmp, w-x, y+1, glow);

        int dir = rand() % 4;
        int val = rand() % 7;
        switch (dir) {
            case 0: x += val; break;
            case 1: y += val; break;
            case 2: x -= val; break;
            case 3: y -= val; break;
        }
        if (!IN_RANGE(x, 0, bmp->width - 40) || !IN_RANGE(y, 0, bmp->height - 40)) {
            x = bmp->width / 2;
            y = bmp->height / 2;
        }
    }
}

/**
 *  TODO http://en.wikipedia.org/wiki/Mandelbrot_set
 */
void mandelbrot(Bitmap* bmp) {
}

/**
 *  http://en.wikipedia.org/wiki/Dragon_curve
 *  lturn (x, y) = (-y, x)
 *  rturn (x, y) = (y, -x)
 */
void dragon(Bitmap* bmp) {
    Color bg, c;
    color_set(&c, 0, 0, 0);
    color_set(&bg, 255, 255, 255);

    for (int y = 0; y < bmp->height; y++) {
        for (int x = 0; x < bmp->width; x++) {
            bmp_set_pixel(bmp, x, y, bg);
        }
    }

    int nx = 0, ny = 1;
    int x = 200, y = 230;
    int cnt = 256;

    for (int i = 0; i < 80000; i++) {
        if (i > cnt) {
            color_set(&c, 0, c.green + 28, c.blue + 28);
            cnt <<= 1;
        }
        bmp_set_pixel(bmp, x, y, c);
        /*
        int lturn = (((i & -i) << 1) & i) != 0 ? -1 : 1;
        int tmp = nx;
        nx = ny * lturn;
        ny = tmp * -lturn;
        // */
        // /*
        int tmp = nx;
        if ((((i & -i) << 1) & i) != 0) {
            // lturn
            nx = -ny;
            ny = tmp;
        } else {
            // rturn
            nx = ny;
            ny = -tmp;
        }
        // */
        x += nx;
        y += ny;
        bmp_set_pixel(bmp, x, y, c);
        x += nx;
        y += ny;
    }
}

void binary(Bitmap* bmp) {
    Color one, zero;
    color_set(&one, 255, 255, 255);
    color_set(&zero, 0, 64, 192);

    for (int y = 0; y < bmp->height; y++) {
        for (int x = 0; x < 32; x++) {
            // 11184700
            bmp_set_pixel(bmp, 31 - x, y, (0 + y) & (1 << x) ? one : zero);
        }
    }
}

/**
 *  TODO http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
 */
void line(Bitmap* bmp) {
    Color bg, c;
    color_set(&bg, 255, 255, 255);
    color_set(&c, 0, 0, 0);

    for (int y = 0; y < bmp->height; y++) {
        for (int x = 0; x < bmp->width; x++) {
            bmp_set_pixel(bmp, x, y, bg);
        }
    }

    int ox, oy;
    ox = bmp->width / 2;
    oy = bmp->height / 2;

    for (int i = -200; i < 200; i++) {
        int x = i;
        int y = 137 / 9.0  - (37 * x) / 9.0;
        bmp_set_pixel(bmp, ox + x, oy - y, c);
    }
}
