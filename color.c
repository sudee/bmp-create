#include <stdio.h>
#include "color.h"

Color color_create(uint8_t r, uint8_t g, uint8_t b) {
    Color c;
    c.red = r;
    c.green = g;
    c.blue = b;
    return c;
}

void color_set(Color* c, uint8_t r, uint8_t g, uint8_t b) {
    c->red = r;
    c->green = g;
    c->blue = b;
}

void color_print(Color* c) {
    printf("0x%02X%02X%02X", c->red, c->green, c->blue);
}

void color_invert(Color* c) {
    c->red   = 255 - c->red;
    c->green = 255 - c->green;
    c->blue  = 255 - c->blue;
}

int color_to_int(Color* c) {
    return c->red << 16 | c->green << 8 | c->blue;
}