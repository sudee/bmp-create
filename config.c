#include <stdio.h>
#include <string.h>
#include "config.h"

int find_arg(char** arr, int len, char* str) {
    for (int i = 1; i < len; i++) {
        if (strcmp(arr[i], str) == 0) return i;
    }
    return -1;
}

void read_arg(char* fs, char* name, void* p, int argc, char** argv) {
    int idx = find_arg(argv, argc, name);
    if (idx != -1 && argv[idx + 1] != NULL) {
        sscanf(argv[idx + 1], fs, p);
    }
}

Config cfg_create(int argc, char** argv) {
    Config c;
    c.width = 800;
    c.height = 600;
    c.seed = 0;
    c.output = "sample.bmp";
    
    // read_arg("%s", "-o", &c.output, argc, argv);
    read_arg("%d", "-s", &c.seed, argc, argv);
    read_arg("%d", "-w", &c.width, argc, argv);
    read_arg("%d", "-h", &c.height, argc, argv);

    return c;
}

void print_info(Config* cfg) {
    puts("BMPCreate v0.01");
    printf("Output file: %s\n", cfg->output);
    printf("Width: %d, Height: %d\n", cfg->width, cfg->height);
    printf("Seed: %d\n", cfg->seed);
}
